<?php

namespace tests\Yar\Controllers;

use tests\Yar\Controllers\Controller;

class Home extends Controller {
    public function index() {
        return "lol";
    }

    protected function cantSee1() {
        return "lol";
    }

    private function cantSee2() {
        return "lol";
    }
}