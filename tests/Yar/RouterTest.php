<?php

use tests\Yar\Controllers\Hanging;
use tests\Yar\Controllers\Home;
use tests\Yar\Controllers\Inventory\Product;
use Yar\Http\Exception\Exception404;
use Yar\Http\Request;
use Yar\Router;

class RouterTest extends \PHPUnit_Framework_TestCase {
    public function getConfig() {
        return [
            "route" => [
                "max_parts" => 1000,
                "namespaces" => ["tests\Yar\Controllers"],
                "base_class" => "tests\Yar\Controllers\Controller",
            ]
        ];
    }

    public function getConfigNoBase() {
        return [
            "route" => [
                "max_parts" => 1000,
                "namespaces" => ["tests\Yar\Controllers"],
            ]
        ];
    }

    public function getRequest() {
        return Request::fromGlobals($this->getConfig());
    }

    public function getRequestNoBase() {
        return Request::fromGlobals($this->getConfigNoBase());
    }

    public function testNoRoute() {
        $request = $this->getRequest();
        unset($request->server["PATH_INFO"]);
        try {
            Router::createCallable($request);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `::`", $err->getMessage());
        }
    }

    public function testNoHomeRoute() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/";
        try {
            Router::createCallable($request);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `::`", $err->getMessage());
        }
    }

    public function testHomeRouteOverriden() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/";
        $callable = Router::createCallable($request, ["" => "home/index"]);
        $this->assertTrue($callable[0] instanceof Home);
        $this->assertEquals("index", $callable[1]);
    }

    public function testHomeRouteOverridenBad() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/";
        try {
            Router::createCallable($request, ["" => "home/cantFind"]);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `Home::cantFind`, `tests\Yar\Controllers\Home` was found but method:`cantFind` does not exist!", $err->getMessage());
        }
    }

    public function testHomeRouteProtected() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/home/cantSee1/";
        try {
            Router::createCallable($request);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `Home::cantSee1`, `tests\Yar\Controllers\Home` was found but method:`cantSee1` is not public!", $err->getMessage());
        }
    }

    public function testHomeRoutePrivate() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/home/cantSee2/";
        try {
            Router::createCallable($request);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `Home::cantSee2`, `tests\Yar\Controllers\Home` was found but method:`cantSee2` is not public!", $err->getMessage());
        }
    }

    public function testNoExistentRoute() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/lol/lolest/";
        try {
            Router::createCallable($request);
            $this->assertEquals(1, 2, "Should not reach here!");
        } catch (Exception404 $err) {
            $this->assertEquals("Can't find controller `Lol::lolest`", $err->getMessage());
        }
    }

    public function testDefaultIndexRoute() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/home/";
        $callable = Router::createCallable($request);
        $this->assertTrue($callable[0] instanceof Home);
        $this->assertEquals("index", $callable[1]);
    }

    public function testNoBaseClassRoute() {
        $request = $this->getRequestNoBase();
        $request->server["PATH_INFO"] = "/hanging/index/";
        $callable = Router::createCallable($request);
        $this->assertTrue($callable[0] instanceof Hanging);
        $this->assertEquals("index", $callable[1]);
    }

    public function testNestedRoute() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/inventory/product/index/";
        $callable = Router::createCallable($request);
        $this->assertTrue($callable[0] instanceof Product);
        $this->assertEquals("index", $callable[1]);
    }

    public function testNestedDefaultIndexRoute() {
        $request = $this->getRequest();
        $request->server["PATH_INFO"] = "/inventory/product/";
        $callable = Router::createCallable($request);
        $this->assertTrue($callable[0] instanceof Product);
        $this->assertEquals("index", $callable[1]);
    }
}