<?php

use Yar\Http\Exception\Exception301;
use Yar\Http\Exception\Exception302;
use Yar\Http\Exception\Exception403;
use Yar\Http\Exception\Exception404;
use Yar\Http\Exception\Exception500;

class ExceptionTest extends \PHPUnit_Framework_TestCase {

    public function test301() {
        $err = new Exception301("/next/point/");
        $this->assertEquals("/next/point/", $err->target);
        $this->assertEquals(301, $err->status);
    }

    public function test302() {
        $err = new Exception302("/next/point/");
        $this->assertEquals("/next/point/", $err->target);
        $this->assertEquals(302, $err->status);
    }

    public function test403() {
        $err = new Exception403("lol");
        $this->assertEquals(403, $err->status);
    }

    public function test404() {
        $err = new Exception404("lol");
        $this->assertEquals(404, $err->status);
    }
    public function test500() {
        $err = new Exception500("lol");
        $this->assertEquals(500, $err->status);
    }
}