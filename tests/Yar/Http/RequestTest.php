<?php

use Yar\Http\Request;

class RequestTest extends \PHPUnit_Framework_TestCase {

    public function getConfig() {
        return [];
    }

    /** @return Yar\Http\Request */
    public function getRequest() {
        return Request::fromGlobals($this->getConfig());
    }

    public function testServerProtocol() {
        $request = $this->getRequest();
        $request->server["SERVER_PROTOCOL"] = "HTTP/1.0";
        $this->assertEquals("HTTP/1.0", $request->serverProtocol());
    }

    public function testIsPost() {
        $request = $this->getRequest();
        $request->server["REQUEST_METHOD"] = "POST";
        $this->assertTrue($request->isPost());
    }

    public function testIsGet() {
        $request = $this->getRequest();
        $request->server["REQUEST_METHOD"] = "GET";
        $this->assertTrue($request->isGet());
    }

    public function testMethod() {
        $request = $this->getRequest();
        $request->server["REQUEST_METHOD"] = "GET";
        $this->assertEquals("GET", $request->method());
    }
}