<?php

use Yar\Http\Request;
use Yar\Http\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase {

    public function getConfig() {
        return [];
    }

    /** @return Yar\Http\Request */
    public function getRequest() {
        return Request::fromGlobals($this->getConfig());
    }

    /** @return Yar\Http\Response */
    public function getResponse(...$args) {
        $request = $this->getRequest();
        return new Response($request, ...$args);
    }

    public function testGetBody() {
        $response = $this->getResponse("lol");
        $this->assertEquals("lol", $response->getBody());
    }

    public function testGetStatus() {
        $response = $this->getResponse();
        $this->assertEquals(200, $response->getStatus());
    }

    public function testGetStatusMessage() {
        $response = $this->getResponse();
        $this->assertEquals("200 OK", $response->getStatusMessage());
    }

    public function testGetHeaders() {
        $request = $this->getRequest();
        $request->server["SERVER_PROTOCOL"] = "HTTP/1.0";
        $response = $this->getResponse("", ["Location" => "/"]);
        $this->assertEquals(["Location" => "/"], $response->getHeaders());
    }

    public function testGetHeader() {
        $request = $this->getRequest();
        $request->server["SERVER_PROTOCOL"] = "HTTP/1.0";
        $response = $this->getResponse("", ["Location" => "/"]);
        $this->assertEquals("HTTP/1.0 200 OK\r\nLocation: /\r\n\r\n", $response->getHeader());
    }
}