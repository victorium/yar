# Yet Another Router (YAR) for PHP

# Introduction

YAR is a very simple routing library for PHP that routes URLs to methods in classes.

## Licence

All source files in YAR are released subject to the terms of the MIT license, as written in the included LICENSE.txt file.

## Installation

### Composer

YAR can be installed with composer (http://getcomposer.org/).

1. Install composer:

	```
	$ curl -s https://getcomposer.org/installer | php
	```

2. Require YAR as a dependency using composer:

	```
	$ php composer.phar require victorium/yar
	```

## Getting Started

This is a simple example on getting started with YAR:

	<?php

    define("PATH_TO_YAR_SRC", dirname(__DIR__));

    require PATH_TO_YAR_SRC . "/bootstrap.php";

    use Yar\Http\Exception\Exception404;
    use Yar\Http\Request;
    use Yar\Router;

    class Home {
        public function index() {
            echo "Hi from index";
        }

        public function sum($a, $b) {
            return $a + $b;
        }
    }

    $config = [
        "route" => [
            "max_parts" => 1000,
            "namespaces" => [""],
        ]
    ];

    $request = Request::fromGlobals($config);

    $override = ["" => "home/index"];
    list($obj, $methodName, $args) = Router::createCallable($request, $override);
    call_user_func([$obj, $methodName], $args);