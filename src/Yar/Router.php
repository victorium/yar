<?php

/*
 * Yar/Router.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar;

use Yar\Http\Exception as HttpException;

class Router {

    public $request;
    public $routes = [];
    public $arguments = [];
    public $nodes = [];

    public function __construct($request, $routes) {
        $this->request = $request;
        $this->routes = $routes;
    }

    /**
     * @return Node
     * @throws \Yar\Http\Exception
     */
    public function find() {
        $currentNode = $this->routeToNodes();
        $prematureExit = false;
        foreach ($this->urlToNodes() as $node) {
            if (isset($currentNode[$node])) {
                $currentNode = &$currentNode[$node];
            } elseif (isset($currentNode["@"]) && is_numeric($node)) {
                $currentNode = &$currentNode["@"];
                $this->arguments[] = $node;
            } elseif (isset($currentNode["*"])) {
                $currentNode = &$currentNode["*"];
                $this->arguments[] = $node;
            } else {
                $prematureExit = true;
                break;
            }
        }
        if (isset($currentNode["$"]) && !$prematureExit) {
            return new Node($currentNode["$"], $this->arguments);
        } else {
            throw new HttpException(404, "Can't find controller for `{$uri}");
        }
    }

    public function urlToNodes() {
        return explode("/", trim($this->request->uri(), "/"));
    }

    public function routeToNodes() {
        $this->nodes = [];
        foreach ($this->routes as $node => $callback) {
            $currentNode = &$this->nodes;
            $sections = explode("/", trim($node, "/"));
            foreach ($sections as $section) {
                if (!isset($currentNode[$section])) {
                    $currentNode[$section] = [];
                }
                $currentNode = &$currentNode[$section];
            }
            $currentNode['$'] = $callback;
        }
        return $this->nodes;
    }

}
