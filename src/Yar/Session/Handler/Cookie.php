<?php

/*
 * Yar/Session/Handler/File.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Session\Handler;

/**
 * Saving session data in a cookie
 */
class Cookie implements \SessionHandlerInterface {

    protected $request;

    public function __construct($request) {
        $this->request = $request;
    }

    public function close() {
        return true;
    }

    public function destroy($id) {
        unset($this->request->cookie[$id]);
    }

    public function open() {
        return true;
    }

    public function gc($maxlifetime) {
        return true;
    }

    public function read($id) {
        return $this->request->cookie[$id] ?? "";
    }

    public function write($id, $data) {
        $this->request->cookie[$id] = $data;
        return true;
    }

}
