<?php

/*
 * Yar/Session/Handler/Db.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yar\Session\Handler;

/*
 * Saving session data in database
 *
 */
class Db implements \SessionHandlerInterface {

    protected $config = null;
    protected $db = null;
    protected $readStmt = null;
    protected $writeStmt = null;
    protected $destroyStmt = null;

    public function __construct($config) {
        $this->config = $config;
    }

    public function read($id) {
        $this->readStmt->execute(["id" => $id]);
        $value = $this->readStmt->fetchColumn();
        $this->readStmt->closeCursor();
        return $value === false ? "" : $value;
    }

    public function write($id, $value) {
        $status = $this->writeStmt->execute(["id" => $id, "value" => $value]);
        $this->writeStmt->closeCursor();
        return $status;
    }

    public function open($savePath, $sessionName) {
        $this->initDb();
        $table = $this->config["session"]["table"] ?? "_session";
        $this->readStmt = $this->db->prepare("SELECT value FROM {$table} WHERE id=:id");
        $this->writeStmt = $this->db->prepare("REPLACE INTO {$table} (id, value) VALUES (:id, :value)");
        $this->destroyStmt = $this->db->prepare("DELETE FROM {$table} WHERE id=:id");
        return true;
    }

    public function close() {
        return true;
    }

    public function destroy($id) {
        $this->destroyStmt->execute(["id" => $id]);
    }

    public function gc($maxlifetime) {
        return true;
    }

    protected function initDb() {
        if ($this->db === null) {
            $c = $this->config["db"];
            if ($c["adapter"] == "sqlite") {
                $this->db = new \PDO("sqlite:{$c['name']}", null, null, $c['options']);
            } else {
                $this->db = new \PDO("{$c['adapter']}:host={$c['host']};port={$c['port']};dbname={$c['name']}", $c['user'], $c['pass'], $c['options']);
            }
        }
    }

}
