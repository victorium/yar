<?php

/*
 * Yar/Session/Handler/File.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Session\Handler;

/**
 * Saving session data in files
 */
class File implements \SessionHandlerInterface {

    protected $config;

    public function __construct($config) {
        $this->config = $config;
    }

    public function close() {
        return true;
    }

    public function destroy($id) {
        $fullFileName = ($config["session"]["path"] ?? "/tmp") . "/" . $id;
        if (is_file($fullFileName)) {
            unlink($fullFileName);
        }
    }

    public function open() {
        return true;
    }

    public function gc($maxlifetime) {
        return true;
    }

    public function read($id) {
        $fullFileName = ($config["session"]["path"] ?? "/tmp") . "/" . $id;
        return file_get_contents($fullFileName);
    }

    public function write($id, $data) {
        $fullFileName = ($config["session"]["path"] ?? "/tmp") . "/" . $id;
        return file_put_contents($fullFileName, $data) !== false;
    }

}
