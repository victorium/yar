<?php

/*
 * Yar/Session/Session.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Session;

/**
 * Session class
 */
class Session implements \ArrayAccess, \Iterator {

    /** @var array */
    protected $data;

    /** @var int */
    protected $position;

    /** @var int */
    protected $expiry = 0;

    /** @var string */
    protected $sessionName;

    /** @var string */
    protected $cookieName;

    /** @var boolean */
    protected $doCookieWrite = false;

    /** @var \Yar\Http\Request */
    protected $request;

    /** @var \SessionHandlerInterface */
    protected $handler;

    public function __construct($request, $handler, $cookieName = "sess") {
        $this->request = $request;
        $this->handler = $handler;
        $this->cookieName = $cookieName;

        if (isset($request->cookie[$this->cookieName])) {
            $this->sessionName = $request->cookie[$this->cookieName];
        } else {
            $this->generateSessionName();
        }
        $this->position = 0;
    }

    //// Iterator implementation 

    public function current() {
        return $this->data[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        $this->position++;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function valid(): bool {
        return isset($this->data[$this->position]);
    }

    //// Iterator implementation
    //
    //
    //// ArrayAccess implementation

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    //// ArrayAccess implementation
    //
    //
    //// Session implementation

    public function open() {
        if ($this->handler->open("/", $this->cookieName)) {
            $this->data = unserialize($this->handler->read($this->sessionName));
        }
    }

    public function close() {
        $this->handler->write($this->sessionName, serialize($this->data));
        $this->handler->close();

        if ($this->doCookieWrite) {
            $this->request->cookie[$this->cookieName] = $this->sessionName;
        }
    }

    public function destroy() {
        $this->handler->destroy($this->sessionName);
        $this->data = [];
    }

    public function regenerateId() {
        $this->destroy();
        $this->generateSessionName();
    }

    public function getData() {
        return $this->data;
    }

    public function setExpiry($expiry) {
        $this->expiry = $expiry;
    }

    protected function generateSessionName() {
        $this->doCookieWrite = true;
        $range = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $token = "sess_";
        for ($i = 0; $i < 32; $i++) {
            $token .= $range[random_int(0, 61)];
        }
        $this->sessionName = $token;
    }

}
