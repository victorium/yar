<?php

/*
 * Yar/Node.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar;

class Node {

    public $callback;
    public $arguments;

    public function __construct($callback, $arguments) {
        $this->callback = $callback;
        $this->arguments = $arguments;
    }

    public function className() {
        if (is_array($this->callback) && is_string($this->callback[0]) && class_exists($this->callback[0])) {
            return $this->callback[0];
        }
    }

    public function methodName() {
        if (is_array($this->callback) && is_string($this->callback[1])) {
            return $this->callback[1];
        }
    }

    /**
     * 
     * @param Http\Request $request
     * @return Http\Response
     */
    public function response($request) {
        $className = $this->className();
        $object = new $className($request);
        $callback = [$object, $this->methodName()];
        return $callback(...$this->arguments);
    }

}
