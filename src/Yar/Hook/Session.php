<?php

/*
 * Yar/Hook/Session.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Hook;

use Yar\Http\IHook;

class Session implements IHook {

    public static function close($response) {
        $response->request->session->close();
    }

    public static function open($request) {
        $request->session = new \Yar\Session\Session($request, new \Yar\Session\Handler\File([]));
        $request->session->open();
    }

}
