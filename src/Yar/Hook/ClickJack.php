<?php

/*
 * Yar/Hook/Expire.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Hook;

use Yar\Http\IHook;

class ClickJack implements IHook {

    public static function close($response) {
        $response->headers["X-FRAME-OPTIONS"] = "SAMEORIGIN";
    }

    public static function open($request) {
        
    }

}
