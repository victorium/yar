<?php

/*
 * Yar/Hook/Cookie.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Hook;

use Yar\Http\IHook;

class Cookie implements IHook {

    public static function close($response) {
        foreach ($response->request->cookie as $name => $value) {
            setcookie($name, $value);
        }
    }

    public static function open($request) {
        
    }

}
