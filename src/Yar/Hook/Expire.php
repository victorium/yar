<?php

/*
 * Yar/Hook/Expire.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */
namespace Yar\Hook;

use Yar\Http\IHook;

class Expire implements IHook {

    public static function close($response) {
        // No caching dynamic content!
        if (!isset($response->headers["Expires"])) {
            $response->headers["Expires"] = "Thu, 19 Nov 1981 08:52:00 GMT";
            $response->headers["Cache-Control"] = "no-store, no-cache, must-revalidate, post-check=0, pre-check=0";
            $response->headers["Pragma"] = "no-cache";
        }
    }

    public static function open($request) {
        
    }

}
