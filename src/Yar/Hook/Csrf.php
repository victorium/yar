<?php

/*
 * Yar/Hook/Csrf.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Hook;

use Yar\Http\IHook;

class Csrf implements IHook {

    public static function close($response) {
        
    }

    public static function open($request) {
        
    }

}
