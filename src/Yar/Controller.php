<?php

/*
 * Yar/Controller.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar;

use Yar\Http\Exception as HttpException;
use Yar\Http\Request;
use Yar\Http\Response;

class Controller {

    /** @var \Yar\Http\Request */
    public $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    /** @return \Yar\Http\Response */
    public function respond($body, $headers = [], $status = 200) {
        return new Response($this->request, $body, $headers, $status);
    }

    /** @return \Yar\Http\Response */
    public function json($context) {
        return $this->respond(json_encode($context), ['Content-type' => "application/json"]);
    }

    /** @throws \Yar\Http\Exception */
    public function redirect($target) {
        throw new HttpException(302, $target);
    }

    /** @throws \Yar\Http\Exception */
    public function notFound($message) {
        throw new HttpException(404, $message);
    }

}
