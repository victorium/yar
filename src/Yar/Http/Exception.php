<?php

/*
 * Yar/Http/Exception.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Http;

/**
 * Exception for directly raising a response
 */
class Exception extends \Exception {

    public $status = null;
    public $target = null;

    public function __construct($status, $target = null) {
        $this->status = $status;
        $this->target = $target;
        parent::__construct(Message::CODES[$status] . ($target ? " " . $target : ""));
    }

}
