<?php

/*
 * Yar/Http/Request.php
 *
 * Copyright: (c) 2016 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Http;

/**
 * Base request class
 */
class Request {

    /** @var array */
    public $server;

    /** @var array */
    public $cookie;

    /** @var array */
    public $query;

    /** @var array */
    public $form;

    /** @var IHook[] */
    public $hooks = [];

    public function __construct($server, $query, $form, $cookie) {
        $this->server = $server;
        $this->query = $query;
        $this->cookie = $cookie;
        $this->form = $form;
        foreach ($this->hooks as $hook) {
            $hook::open($this);
        }
    }

    /**
     * Return whether HTTP request is $method
     *
     * @return boolean
     */
    public function is($method) {
        return $this->server["REQUEST_METHOD"] == strtoupper($method);
    }

    /** @return string URI */
    public function uri() {
        $key = "REQUEST_URI";
        if (isset($this->server[$key]) && $this->server[$key] != "") {
            $uri = $this->server[$key];
            $queryPos = strpos($uri, "?");
            if ($queryPos !== false) {
                $uri = substr($uri, 0, $queryPos);
            }
            return str_replace("..", "", $uri);
        }
        return "/";
    }

    /** @return \static */
    public static function fromGlobals() {
        return new static($_SERVER, $_GET, array_merge($_POST, $_FILES), $_COOKIE);
    }

}
