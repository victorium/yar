<?php

/*
 * Yar/Http/Response.php
 *
 * Copyright: (c) 2016 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Http;

/**
 * Base response class
 */
class Response {

    /** @var Request */
    public $request;

    /** @var array */
    public $headers;

    /** @var int */
    public $status;

    /** @var string */
    public $body;

    public function __construct($request, $body = "", $headers = [], $status = 200) {
        $this->request = $request;
        $this->body = $body;
        $this->headers = $headers;
        $this->status = (int) $status;
    }

    public function sendHeaders() {
        //header($this->request->serverProtocol() . " " . Message::CODES[$this->status]);
        foreach ($this->headers as $key => $value) {
            header("{$key}: {$value}");
        }
    }

    public function content() {
        $this->sendHeaders();
        foreach ($this->request->hooks as $hook) {
            $hook::close($this);
        }
        return $this->body;
    }

}
