<?php

/*
 * Yar/Http/IHook.php
 *
 * Copyright: (c) 2018 Victorium Holdings LTD
 * License: Subject to the terms of the MIT license, as written in the included LICENSE.txt file.
 * Authors: Joseph N. Mutumi
 */

namespace Yar\Http;

interface IHook {

    public static function open($request);

    public static function close($reponse);
}
