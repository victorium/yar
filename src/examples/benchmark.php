<?php

define("PATH_TO_YAR_SRC", dirname(__DIR__));

require PATH_TO_YAR_SRC . "/bootstrap.php";

use Yar\Http\Request;
use Yar\Router;

class Home {
    public function index() {
        echo "Hi from index";
    }
}

$routes = [
    "/" => [Home::class, "index"],
];

$request = Request::fromGlobals();
$start = microtime(true);
for ($i=0; $i<10000; $i++) {
    $router = new Router($request, $routes);
    $node = $router->find();
}
printf("%s times took %.6fs\n", number_format($i), microtime(true) - $start);