<?php

define("PATH_TO_YAR_SRC", dirname(__DIR__));

require PATH_TO_YAR_SRC . "/bootstrap.php";

use Yar\Http\Request;
use Yar\Router;

class Home {

    public function index() {
        echo "Hi from index";
    }

}

function sum($a, $b) {
    echo $a + $b;
}

$routes = [
    "/" => [Home::class, "index"],
    "/sum/@/@/" => "sum",
];

$request = Request::fromGlobals();
//$request->server["REQUEST_URI"] = "/sum/1/2/";
try {
    $router = new Router($request, $routes);
    $node = $router->find();
    $className = $node->className();
    if ($className) {
        $object = new $className();
        $callback = [$object, $node->methodName()];
    } else {
        $callback = $node->callback;
    }
    $callback(...$node->arguments);
} catch (Exception $err) {
    var_dump($err->getMessage());
}
