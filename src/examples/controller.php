<?php

define("PATH_TO_YAR_SRC", dirname(__DIR__));

require PATH_TO_YAR_SRC . "/bootstrap.php";

use Yar\Controller;
use Yar\Http\Request;
use Yar\Router;

class Home extends Controller {

    public function index() {
        return $this->respond("Hi from index");
    }

    public function sum($a, $b) {
        return $this->respond($a + $b);
    }

}

$routes = [
    "/" => [Home::class, "index"],
    "/sum/@/@/" => [Home::class, "sum"],
];

$request = Request::fromGlobals();
//$request->server["REQUEST_URI"] = "/sum/1/2/";
try {
    $router = new Router($request, $routes);
    $node = $router->find();
    echo $node->response($request)->content();
} catch (Exception $err) {
    var_dump($err->getMessage());
}
